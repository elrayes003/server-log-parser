
CREATE TABLE if not EXISTS REQUEST_LOG
(
  submission_date varchar(45) NOT NULL,
  source_ip varchar(45) NOT NULL,
  request varchar(45) NOT NULL,
  status varchar(45) NOT NULL,
  user_agent varchar(200) DEFAULT NULL,
);

CREATE TABLE `BLOCKED_IP` (
  `ip` varchar(45) NOT NULL,
  `report_start_date` datetime NOT NULL,
  `report_end_date` datetime NOT NULL,
  `hits` bigint(20) NOT NULL,
  `blocked_date` datetime NOT NULL,
  `block_threshold` int(11) NOT NULL,
);

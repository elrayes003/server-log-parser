package com.wallethub.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class DateUtil {
    final static String REQUEST_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String stringDateAddition(String date, String duration) throws ParseException {
        Objects.requireNonNull(date, "missing date argument");
        Objects.requireNonNull(duration, "missing duration argument");

        SimpleDateFormat formatter = new SimpleDateFormat(REQUEST_DATE_FORMAT);
        GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(formatter.parse(date));


        if (duration.toLowerCase().equals(Duration.hourly.name()))
            cal.add(Calendar.HOUR, 1);
        else if (duration.equals(Duration.daily.name()))
            cal.add(Calendar.DATE, 1);
        else
            throw new IllegalArgumentException("invalid supplied duration");

        return formatter.format(cal.getTime());
    }
}

package com.wallethub.application;

import com.wallethub.configuration.AppConfig;
import com.wallethub.controller.Controller;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Objects;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class Parser {
    public static void main(String[] args) throws Exception {
//        args = new String[4]; // for testing purposes
//        args[0] = "--startDate=2017-01-01.15:00:00";
//        args[1] = "--duration=hourly";
//        args[2] = "--threshold=200";
//        args[3]= "--accesslog=/home/elrayes/Desktop/test-log";

        String accessLog = null;
        String startDate = null;
        String duration = null;
        Integer threshold = null;

        for (String str : args) {
            String[] pair = str.split("=");
            if (pair.length != 2)
                throw new IllegalArgumentException();
            String key = pair[0].replace("-", "");
            switch (key.toLowerCase()) {
                case "accesslog":
                    accessLog = pair[1];
                    break;
                case "startdate":
                    startDate = pair[1].replace(".", " ");
                    break;
                case "duration":
                    duration = pair[1];
                    break;
                case "threshold":
                    threshold = Integer.parseInt(pair[1]);
                    break;
                default:
                    throw new IllegalArgumentException("unknown arg " + key);
            }
        }

        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        final Controller controller = (Controller) context.getBean("controller");

        boolean loadingLogs = false;
        boolean findingSuspiciousIps = false;

        if (accessLog != null && !accessLog.isEmpty()) {
            controller.logRequests(accessLog);
            loadingLogs = true;
        }

        if (Objects.nonNull(startDate) && Objects.nonNull(duration) && Objects.nonNull(threshold)) {
            controller.blockSuspiciousIps(startDate, duration, threshold);
            findingSuspiciousIps = true;
        }
        if (!loadingLogs && !findingSuspiciousIps)
            throw new IllegalArgumentException("did nothing ! , make sure about setting accesslog Or (startDate, duration and threshold) values..");

    }

}

package com.wallethub.controller;

import com.wallethub.model.BlockedIp;
import com.wallethub.model.RequestLog;
import org.springframework.beans.factory.annotation.Autowired;
import com.wallethub.service.BlockedIpService;
import com.wallethub.service.RequestLogService;
import com.wallethub.utils.DateUtil;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.wallethub.controller.RequestLogParser.parseFile;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class Controller {

    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    @Autowired
    RequestLogService requestLogService;

    @Autowired
    BlockedIpService blockedIpService;

    public void logRequests(String filePath) throws IOException, ParseException {
        Objects.requireNonNull(filePath);

        final long startTime = System.nanoTime();
        List<RequestLog> logs = parseFile(filePath);
        requestLogService.addRequestLogs(logs);
        final long duration = System.nanoTime() - startTime;

        LOGGER.log(Level.INFO, MessageFormat.format("Loaded successfully {0} logs record with running time {1} s", logs.size(),
                duration / 1000000000.0));
    }

    public void blockSuspiciousIps(String startDate, String duration, int threshold) throws ParseException {
        Objects.requireNonNull(startDate);
        Objects.requireNonNull(duration);

        String toDate = DateUtil.stringDateAddition(startDate, duration);
        List<BlockedIp> suspiciousIps = requestLogService.findSuspiciousIps(startDate, toDate, threshold);
        logBlockedIps(suspiciousIps);
        blockedIpService.addBlockedIps(suspiciousIps);
    }

    private void logBlockedIps(List<BlockedIp> suspiciousIps) {
        if (suspiciousIps == null || suspiciousIps.size() == 0)
            return;
        System.out.println("================== Blocked IPs ==================");
        for (int i = 0; i < suspiciousIps.size(); i++)
            System.out.println(i + 1 + "- " + suspiciousIps.get(i).getIp());
        System.out.println("=================================================");
    }
}

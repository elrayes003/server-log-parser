package com.wallethub.controller;

import com.wallethub.model.RequestLog;

import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class RequestLogParser {
    private static final Logger LOGGER = Logger.getLogger(RequestLogParser.class.getName());

    public static List<RequestLog> parseFile(String filePath) throws IOException, ParseException {
        List<RequestLog> requestLogs = new ArrayList<RequestLog>();
        FileInputStream inputStream = null;
        Scanner sc = null;

        try {
            inputStream = new FileInputStream(filePath);
            sc = new Scanner(inputStream, "UTF-8");
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] lineParts = line.split("\\|");

                if (lineParts.length != 5) {
                    LOGGER.log(Level.WARNING, "Invalid data for request log record in line number: {0}", requestLogs.size());
                }

                RequestLog requestLog = new RequestLog();
                requestLog.setSubmissionDate(lineParts[0]);
                requestLog.setSourceIp(lineParts[1]);
                requestLog.setRequest(lineParts[2]);
                requestLog.setStatus(lineParts[3]);
                requestLog.setUserAgent(lineParts[4]);

                requestLogs.add(requestLog);
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }
        return requestLogs;
    }
}

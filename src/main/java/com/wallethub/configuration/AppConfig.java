package com.wallethub.configuration;

import com.wallethub.controller.Controller;
import com.wallethub.dao.BlockedIpDAO;
import com.wallethub.dao.BlockedIpDAOImpl;
import com.wallethub.dao.RequestLogDAO;
import com.wallethub.dao.RequestLogDAOImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.wallethub.service.BlockedIpService;
import com.wallethub.service.BlockedIpServiceImpl;
import com.wallethub.service.RequestLogService;
import com.wallethub.service.RequestLogServiceImpl;

import javax.sql.DataSource;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
@Configuration
@ComponentScan({"com.wallethub"})
@PropertySource(value = {"classpath:application.properties"})
public class AppConfig {

    @Autowired
    private Environment environment;
    @Bean
    public DataSource dataSource()  {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url")+"?rewriteBatchedStatements=true");
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    @Bean
    public RequestLogDAO requestLogDAO() {
        RequestLogDAOImpl requestLogDAO = new RequestLogDAOImpl(dataSource());
        return requestLogDAO;
    }

    @Bean
    public BlockedIpDAO blockedIpDAO() {
        BlockedIpDAOImpl blockedIpDAO = new BlockedIpDAOImpl(dataSource());
        return blockedIpDAO;
    }

    @Bean
    public RequestLogService requestLogService() {
        RequestLogServiceImpl requestLogService = new RequestLogServiceImpl();
        return requestLogService;
    }

    @Bean
    public BlockedIpService blockedIpService() {
        BlockedIpServiceImpl blockedIpService = new BlockedIpServiceImpl();
        return blockedIpService;
    }

    @Bean
    public Controller controller() {
        Controller controller = new Controller();
        return controller;
    }
}


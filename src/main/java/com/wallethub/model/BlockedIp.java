package com.wallethub.model;

import java.io.Serializable;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class BlockedIp implements Serializable {
    private String ip;
    private String reportStartDate;
    private String reportEndDate;
    private Long hits;
    private String blockedDate;
    private Integer blockThreshold;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Long getHits() {
        return hits;
    }

    public void setHits(Long hits) {
        this.hits = hits;
    }

    public Integer getBlockThreshold() {
        return blockThreshold;
    }

    public void setBlockThreshold(Integer blockThreshold) {
        this.blockThreshold = blockThreshold;
    }

    public String getReportStartDate() {
        return reportStartDate;
    }

    public void setReportStartDate(String reportStartDate) {
        this.reportStartDate = reportStartDate;
    }

    public String getReportEndDate() {
        return reportEndDate;
    }

    public void setReportEndDate(String reportEndDate) {
        this.reportEndDate = reportEndDate;
    }

    public String getBlockedDate() {
        return blockedDate;
    }

    public void setBlockedDate(String blockedDate) {
        this.blockedDate = blockedDate;
    }
}

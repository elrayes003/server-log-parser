package com.wallethub.dao;

import com.google.common.collect.Lists;
import com.wallethub.model.BlockedIp;
import com.wallethub.model.RequestLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class RequestLogDAOImpl implements RequestLogDAO {
    final int BATCH_SIZE = 10000;

    @Autowired
    BlockedIpDAO blockedIpDAO;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public RequestLogDAOImpl(DataSource dataSource){
        jdbcTemplate= new JdbcTemplate(dataSource);
    }

    public void insert(RequestLog requestLog) {
        String sql = "INSERT INTO REQUEST_LOG " +
                "( submission_date, source_ip,request ,status, user_agent ) VALUES (?, ?, ?,?,?)";

        jdbcTemplate.update(sql, new Object[]{
                requestLog.getSubmissionDate(), requestLog.getSourceIp(), requestLog.getRequest(),
                requestLog.getStatus(), requestLog.getUserAgent()
        });
    }

    public List<RequestLog> findByIp(String ip) {
        String sql = "SELECT * FROM REQUEST_LOG WHERE source_ip = '" + ip + "'";

        List<RequestLog> requestLogs = new ArrayList<RequestLog>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            RequestLog requestLog = extractRequestLogFromRow(row);
            requestLogs.add(requestLog);
        }
        return requestLogs;
    }

    public List<BlockedIp> findSuspiciousIps(String from, String to, Integer threshold) throws ParseException {
        String sql = MessageFormat.format("SELECT source_ip as ip ,{0} as report_start_date, {1} as report_end_date," +
                " count(*) as hits, Now() as blocked_date, {2} as block_threshold " +
                "FROM REQUEST_LOG WHERE submission_date between {0} AND {1} " +
                "group by source_ip" +
                " having hits >= {2} ", "'".concat(from).concat("'"), "'".concat(to).concat("'"), threshold);

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        List<BlockedIp> blockedIps = new ArrayList<BlockedIp>();

        for (Map row : rows) {
            BlockedIp blockedIp = blockedIpDAO.extractBlockedIpFromRow(row);
            blockedIps.add(blockedIp);
        }
        return blockedIps;
    }

    public List<RequestLog> findAll() throws ParseException {
        String sql = "SELECT * FROM REQUEST_LOG";

        List<RequestLog> requestLogs = new ArrayList<RequestLog>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            RequestLog requestLog = extractRequestLogFromRow(row);
            requestLogs.add(requestLog);
        }
        return requestLogs;
    }

    public void insertBatchByRequestLogs(final List<RequestLog> requestLogs) {
        String sql = "INSERT INTO REQUEST_LOG " +
                "( submission_date, source_ip, request,status, user_agent ) VALUES ( ?, ?, ?,?,?)";

        List<List<RequestLog>> subSets = Lists.partition(requestLogs, BATCH_SIZE);

        for (final List<RequestLog> logsList : subSets) {
            jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    RequestLog requestLog = logsList.get(i);
                    ps.setString(1, requestLog.getSubmissionDate());
                    ps.setString(2, requestLog.getSourceIp());
                    ps.setString(3, requestLog.getRequest());
                    ps.setString(4, requestLog.getStatus());
                    ps.setString(5, requestLog.getUserAgent());
                }

                public int getBatchSize() {
                    return logsList.size();
                }
            });
        }
    }

    public void insertBatchBySqlStatement(final String sql) {
        jdbcTemplate.batchUpdate(new String[]{sql});
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    private RequestLog extractRequestLogFromRow(Map row) {
        RequestLog requestLog = new RequestLog();
        requestLog.setSubmissionDate(String.valueOf(row.get("submission_date")));
        requestLog.setSourceIp(String.valueOf(row.get("source_ip")));
        requestLog.setRequest(String.valueOf(row.get("request")));
        requestLog.setStatus(String.valueOf(row.get("status")));
        requestLog.setUserAgent(String.valueOf(row.get("user_agent")));
        return requestLog;
    }
}

package com.wallethub.dao;

import com.google.common.collect.Lists;
import com.wallethub.model.BlockedIp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class BlockedIpDAOImpl implements BlockedIpDAO {
    final int BATCH_SIZE = 10000;

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public BlockedIpDAOImpl(DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public void insert(BlockedIp blockedIp) {
        String sql = "INSERT INTO BLOCKED_IP " +
                "(ip, report_start_date, report_end_date, hits , blocked_date, block_threshold ) VALUES (?, ?, ?, ?,?,?)";

        jdbcTemplate.update(sql, new Object[]{blockedIp.getIp(),
                blockedIp.getReportStartDate(), blockedIp.getReportEndDate(), blockedIp.getHits(),
                blockedIp.getBlockedDate(), blockedIp.getBlockThreshold()
        });
    }

    public List<BlockedIp> findByIp(String ip) {
        String sql = "SELECT * FROM BLOCKED_IP WHERE ip = '" + ip + "'";

        List<BlockedIp> blockedIps = new ArrayList<>();
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            BlockedIp requestLog = extractBlockedIpFromRow(row);
            blockedIps.add(requestLog);
        }
        return blockedIps;
    }

    public List<BlockedIp> findAll() throws ParseException {
        String sql = "SELECT * FROM BLOCKED_IP";

        List<BlockedIp> blockedIps = new ArrayList<BlockedIp>();

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map row : rows) {
            BlockedIp blockedIp = extractBlockedIpFromRow(row);
            blockedIps.add(blockedIp);
        }
        return blockedIps;
    }

    public BlockedIp extractBlockedIpFromRow(Map row) {
        BlockedIp blockedIp = new BlockedIp();
        blockedIp.setIp(String.valueOf(row.get("ip")));
        blockedIp.setReportStartDate(String.valueOf(row.get("report_start_date")));
        blockedIp.setReportEndDate(String.valueOf(row.get("report_end_date")));

        blockedIp.setHits(Long.valueOf(String.valueOf(row.get("hits"))));
        blockedIp.setBlockedDate(String.valueOf(row.get("blocked_date")));
        blockedIp.setBlockThreshold(Integer.valueOf(String.valueOf(row.get("block_threshold"))));
        return blockedIp;
    }

    public void insertBatchByBlockedIps(final List<BlockedIp> blockedIps) {
        String sql = "INSERT INTO BLOCKED_IP " +
                "(ip, report_start_date, report_end_date, hits , blocked_date, block_threshold ) VALUES (?, ?, ?, ?,?,?)";

        List<List<BlockedIp>> subSets = Lists.partition(blockedIps, BATCH_SIZE);

        for (final List<BlockedIp> ipsList : subSets) {
            jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
                public void setValues(PreparedStatement ps, int i) throws SQLException {
                    BlockedIp blockedIp = ipsList.get(i);
                    ps.setString(1, blockedIp.getIp());
                    ps.setString(2, blockedIp.getReportStartDate());
                    ps.setString(3, blockedIp.getReportEndDate());
                    ps.setLong(4, blockedIp.getHits());
                    ps.setString(5, blockedIp.getBlockedDate());
                    ps.setInt(6, blockedIp.getBlockThreshold());
                }

                public int getBatchSize() {
                    return ipsList.size();
                }
            });
        }
    }

    public void insertBatchBySqlStatement(final String sql) {
        jdbcTemplate.batchUpdate(new String[]{sql});
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}

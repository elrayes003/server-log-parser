package com.wallethub.dao;

import com.wallethub.model.BlockedIp;
import com.wallethub.model.RequestLog;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public interface RequestLogDAO {
	public void insert(RequestLog requestLog);

	public List<RequestLog> findAll() throws ParseException;

	public void insertBatchByRequestLogs(final List<RequestLog> logs);

	public void insertBatchBySqlStatement(final String sql);

	public List<BlockedIp> findSuspiciousIps(String from, String to, Integer threshold) throws ParseException;

	public  List<RequestLog>  findByIp(String ip);

	public JdbcTemplate getJdbcTemplate();
}

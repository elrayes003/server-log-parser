package com.wallethub.dao;

import com.wallethub.model.BlockedIp;
import org.springframework.jdbc.core.JdbcTemplate;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public interface BlockedIpDAO {
	public void insert(BlockedIp blockedIp);

	public List<BlockedIp> findByIp(String ip);

	public List<BlockedIp> findAll() throws ParseException;

	public void insertBatchByBlockedIps(final List<BlockedIp> logs);

	public void insertBatchBySqlStatement(final String sql);

	public JdbcTemplate getJdbcTemplate();

	public BlockedIp extractBlockedIpFromRow(Map row);
}

package com.wallethub.service;

import com.wallethub.model.BlockedIp;
import com.wallethub.model.RequestLog;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public interface RequestLogService {
    public void addRequestLogs(final List<RequestLog> logs);
    public List<BlockedIp> findSuspiciousIps(String from, String to, Integer threshold) throws ParseException;
}

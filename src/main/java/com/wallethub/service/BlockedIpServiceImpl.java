package com.wallethub.service;

import com.wallethub.dao.BlockedIpDAO;
import com.wallethub.model.BlockedIp;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class BlockedIpServiceImpl implements BlockedIpService {
    @Autowired
    BlockedIpDAO blockedIpDAO;

    public void addBlockedIps(final List<BlockedIp> ips) {
        blockedIpDAO.insertBatchByBlockedIps(ips);
    }
}

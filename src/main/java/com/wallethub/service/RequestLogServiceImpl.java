package com.wallethub.service;

import com.wallethub.dao.RequestLogDAO;
import com.wallethub.model.BlockedIp;
import com.wallethub.model.RequestLog;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.util.List;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class RequestLogServiceImpl implements RequestLogService {

    @Autowired
    RequestLogDAO requestLogDAO;

    public void addRequestLogs(final List<RequestLog> logs) {
        requestLogDAO.insertBatchByRequestLogs(logs);
    }

    public List<BlockedIp> findSuspiciousIps(String from, String to, Integer threshold) throws ParseException {
        return requestLogDAO.findSuspiciousIps(from, to, threshold);
    }
}

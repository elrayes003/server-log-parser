package com.wallethub.service;

import com.wallethub.model.BlockedIp;

import java.util.List;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public interface BlockedIpService {
    public void addBlockedIps(final List<BlockedIp> ips);
}

package com.wallethub.dao;

import com.wallethub.configuration.AppConfigTest;
import com.wallethub.model.BlockedIp;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfigTest.class)
public class BlockedIpDaoTests {

    @Autowired
    private BlockedIpDAO blockedIpDAO;

    @Before
    public void before() {
        String insertStatement = "INSERT INTO BLOCKED_IP" +
                "(ip, report_start_date, report_end_date," +
                "hits, blocked_date, block_threshold)" +
                "VALUES" +
                "('192.168.106.134', '2017-01-01 15:00:00', '2017-01-01 15:59:59',696, '2017-11-11 18:57:01', 200)," +
                "('192.168.11.231','2017-01-01 15:00:00','2017-01-01 15:59:59',633, '2017-11-11 18:57:01', 200)," +
                " ('192.168.106.134', '2017-01-01 15:00:00', '2017-01-01 16:00:00',1624, '2017-11-12 18:31:38', 200)," +
                "('192.168.11.231','2017-01-01 15:00:00','2017-01-01 16:00:00', 1477,'2017-11-12 18:31:38', 200)," +
                "('192.168.129.191','2017-01-01 15:00:00', '2017-01-01 16:00:00', 224, '2017-11-12 18:31:38', 200);";
        blockedIpDAO.insertBatchBySqlStatement(insertStatement);
    }

    @After
    public void after() {
        JdbcTestUtils.deleteFromTables(blockedIpDAO.getJdbcTemplate(), "BLOCKED_IP");
    }

    @Test
    public void testInsertAndFindByIp() {
        JdbcTestUtils.deleteFromTables(blockedIpDAO.getJdbcTemplate(), "BLOCKED_IP");
        BlockedIp blockedIp = new BlockedIp();
        blockedIp.setIp("192.168.203.111");
        blockedIp.setReportStartDate("2017-01-01 15:00:00");
        blockedIp.setReportEndDate("2017-01-01 16:00:00");
        blockedIp.setHits(217l);
        blockedIp.setBlockedDate("2017-11-12 18:55:16");
        blockedIp.setBlockThreshold(200);

        blockedIpDAO.insert(blockedIp);
        List<BlockedIp> blockedIpByIp = blockedIpDAO.findByIp(blockedIp.getIp());
        Assert.assertNotNull(blockedIpByIp);
        Assert.assertTrue(blockedIpByIp.size() == 1);
        Assert.assertTrue(blockedIp.getIp().equals(blockedIpByIp.get(0).getIp()));
    }

    @Test
    public void testFindAll() throws ParseException {
        int rowsCount = JdbcTestUtils.countRowsInTable(blockedIpDAO.getJdbcTemplate(), "BLOCKED_IP");
        Assert.assertTrue(blockedIpDAO.findAll().size() == rowsCount);
    }

    @Test
    public void testInsertBatchByRequestLogs() throws ParseException {
        List<BlockedIp> blockedIps = new ArrayList<>();
        BlockedIp blockedIp1 = new BlockedIp();
        blockedIp1.setIp("192.168.203.111");
        blockedIp1.setReportStartDate("2017-01-01 15:00:00");
        blockedIp1.setReportEndDate("2017-01-01 16:00:00");
        blockedIp1.setHits(217l);
        blockedIp1.setBlockedDate("2017-11-12 18:55:16");
        blockedIp1.setBlockThreshold(200);
        blockedIps.add(blockedIp1);

        BlockedIp blockedIp2 = new BlockedIp();
        blockedIp2.setIp("192.168.203.111");
        blockedIp2.setReportStartDate("2017-01-01 15:00:00");
        blockedIp2.setReportEndDate("2017-01-01 16:00:00");
        blockedIp2.setHits(217l);
        blockedIp2.setBlockedDate("2017-11-12 18:55:16");
        blockedIp2.setBlockThreshold(200);
        blockedIps.add(blockedIp2);

        BlockedIp blockedIp3 = new BlockedIp();
        blockedIp3.setIp("192.168.203.111");
        blockedIp3.setReportStartDate("2017-01-01 15:00:00");
        blockedIp3.setReportEndDate("2017-01-01 16:00:00");
        blockedIp3.setHits(217l);
        blockedIp3.setBlockedDate("2017-11-12 18:55:16");
        blockedIp3.setBlockThreshold(200);
        blockedIps.add(blockedIp3);

        blockedIpDAO.insertBatchByBlockedIps(blockedIps);
        Assert.assertTrue(blockedIpDAO.findAll().size() == 8);
    }
}
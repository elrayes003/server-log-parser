package com.wallethub.dao;

import com.wallethub.configuration.AppConfigTest;
import com.wallethub.model.BlockedIp;
import com.wallethub.model.RequestLog;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = AppConfigTest.class)
public class RequestLogDaoTests {

    @Autowired
    private RequestLogDAO requestLogDAO;

    @Before
    public void before() {
        String insertStatement = "INSERT INTO REQUEST_LOG (submission_date, source_ip, request, status, user_agent)\n" +
                "VALUES ('2017-01-01 15:00:23.020', '192.168.94.119', 'GET / HTTP/1.1', '200', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36')," +
                "        ('2017-01-01 15:00:23.094', '192.168.56.125', '\"GET / HTTP/1.1\"', '200', '\"Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36')," +
                "        ('2017-01-01 15:00:26.746', '192.168.56.125', '\"GET / HTTP/1.1\"', '200', '\"Mozilla/5.0 (Windows NT 10.0; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0\"')," +
                "        ('2017-01-01 15:00:27.319', '192.168.56.125', '\"GET / HTTP/1.1\"', '200', '\"Mozilla/5.0 (iPad; CPU OS 9_1 like Mac OS X) AppleWebKit/601.1.46')," +
                "        (' 2017-01-01 15:00:28.121', '192.168.114.124', 'GET / HTTP/1.1', '200', '\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8');";
        requestLogDAO.insertBatchBySqlStatement(insertStatement);
    }

    @After
    public void after() {
        JdbcTestUtils.deleteFromTables(requestLogDAO.getJdbcTemplate(), "REQUEST_LOG");
    }

    @Test
    public void testInsertAndFindByIp() {
        JdbcTestUtils.deleteFromTables(requestLogDAO.getJdbcTemplate(), "REQUEST_LOG");
        RequestLog requestLog = new RequestLog();
        requestLog.setSubmissionDate("2017-01-01 15:00:23.094");
        requestLog.setSourceIp("192.168.228.5");
        requestLog.setRequest("GET / HTTP/1.1");
        requestLog.setStatus("200");
        requestLog.setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36");

        requestLogDAO.insert(requestLog);
        List<RequestLog> requestLogByIp = requestLogDAO.findByIp(requestLog.getSourceIp());
        Assert.assertNotNull(requestLogByIp);
        Assert.assertTrue(requestLogByIp.size() == 1);
        Assert.assertTrue(requestLog.getSourceIp().equals(requestLogByIp.get(0).getSourceIp()));
    }

    @Test
    public void testFindAll() throws ParseException {
        int rowsCount = JdbcTestUtils.countRowsInTable(requestLogDAO.getJdbcTemplate(), "REQUEST_LOG");
        Assert.assertTrue(requestLogDAO.findAll().size() == rowsCount);
    }

    @Test
    public void testInsertBatchByRequestLogs() throws ParseException {
        List<RequestLog> logsList = new ArrayList<>();
        RequestLog requestLog = new RequestLog();
        requestLog.setSubmissionDate("2017-01-01 15:00:23.094");
        requestLog.setSourceIp("192.168.228.5");
        requestLog.setRequest("GET / HTTP/1.1");
        requestLog.setStatus("200");
        requestLog.setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36");
        logsList.add(requestLog);

        requestLog = new RequestLog();
        requestLog.setSubmissionDate("2017-01-01 15:00:33.126");
        requestLog.setSourceIp("192.168.106.181");
        requestLog.setRequest("GET / HTTP/1.1");
        requestLog.setStatus("200");
        requestLog.setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36");
        logsList.add(requestLog);

        requestLog = new RequestLog();
        requestLog.setSubmissionDate("2017-01-01 15:00:33.194");
        requestLog.setSourceIp("192.168.226.6");
        requestLog.setRequest("GET / HTTP/1.1");
        requestLog.setStatus("200");
        requestLog.setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36");
        logsList.add(requestLog);

        requestLogDAO.insertBatchByRequestLogs(logsList);
        Assert.assertTrue(requestLogDAO.findAll().size() == 8);
    }

    @Test
    public void testFindSuspiciousIps() throws ParseException {
        List<BlockedIp> suspiciousIps = requestLogDAO.findSuspiciousIps("2017-01-01 15:00:23", "2017-01-01 15:00:27", 2);
        Assert.assertNotNull(suspiciousIps);
        Assert.assertTrue(suspiciousIps.size() == 1);
        Assert.assertTrue(suspiciousIps.get(0).getIp().equals("192.168.56.125"));
        Assert.assertTrue(suspiciousIps.get(0).getHits() == 2);
    }
}
package com.wallethub.service;

import com.wallethub.dao.BlockedIpDAO;
import com.wallethub.model.BlockedIp;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class BlockedIpImplTest {
    @Mock
    BlockedIpDAO blockedIpDAO;

    @InjectMocks
    BlockedIpServiceImpl blockedIpService;

    @Spy
    List<BlockedIp> blockedIpList = new ArrayList<>();

    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        initialBlockedIpList();
    }

    @Test
    public void testAddBlockedIps() {
        doNothing().when(blockedIpDAO).insertBatchByBlockedIps(blockedIpList);
        blockedIpService.addBlockedIps(blockedIpList);
        verify(blockedIpDAO, atLeastOnce()).insertBatchByBlockedIps(blockedIpList);
    }

    public void initialBlockedIpList() {
        BlockedIp blockedIp1 = new BlockedIp();
        blockedIp1.setIp("192.168.203.111");
        blockedIp1.setReportStartDate("2017-01-01 15:00:00");
        blockedIp1.setReportEndDate("2017-01-01 16:00:00");
        blockedIp1.setHits(217l);
        blockedIp1.setBlockedDate("2017-11-12 18:55:16");
        blockedIp1.setBlockThreshold(200);

        BlockedIp blockedIp2 = new BlockedIp();
        blockedIp2.setIp("192.168.203.111");
        blockedIp2.setReportStartDate("2017-01-01 15:00:00");
        blockedIp2.setReportEndDate("2017-01-01 16:00:00");
        blockedIp2.setHits(217l);
        blockedIp2.setBlockedDate("2017-11-12 18:55:16");
        blockedIp2.setBlockThreshold(200);

        BlockedIp blockedIp3 = new BlockedIp();
        blockedIp3.setIp("192.168.203.111");
        blockedIp3.setReportStartDate("2017-01-01 15:00:00");
        blockedIp3.setReportEndDate("2017-01-01 16:00:00");
        blockedIp3.setHits(217l);
        blockedIp3.setBlockedDate("2017-11-12 18:55:16");
        blockedIp3.setBlockThreshold(200);

        blockedIpList.add(blockedIp1);
        blockedIpList.add(blockedIp2);
        blockedIpList.add(blockedIp3);
    }
}
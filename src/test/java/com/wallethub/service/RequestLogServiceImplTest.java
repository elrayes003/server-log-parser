package com.wallethub.service;

import com.wallethub.dao.RequestLogDAO;
import com.wallethub.model.BlockedIp;
import com.wallethub.model.RequestLog;
import org.junit.Assert;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;

/**
 * Created by Mostafa Elrayes on 11/10/17.
 */
public class RequestLogServiceImplTest {
    @Mock
    RequestLogDAO requestLogDAO;

    @InjectMocks
    RequestLogServiceImpl requestLogService;

    @Spy
    List<RequestLog> requestLogList = new ArrayList<>();

    @Spy
    List<BlockedIp> blockedIpList = new ArrayList<>();

    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        initialRequestLogList();
        initialBlockedIpList();
    }

    @Test
    public void testAddRequestLogs() {
        doNothing().when(requestLogDAO).insertBatchByRequestLogs(requestLogList);
        requestLogService.addRequestLogs(requestLogList);
        verify(requestLogDAO, atLeastOnce()).insertBatchByRequestLogs(requestLogList);
    }

    @Test
    public void testFindSuspiciousIps() throws ParseException {
        List<RequestLog> requestLogs = requestLogList;
        when(requestLogDAO.findSuspiciousIps("2017-01-01 15:00:23", "2017-01-01 15:00:50", 5)).thenReturn(blockedIpList);
        List<BlockedIp> blockedIpsResult = requestLogService.findSuspiciousIps("2017-01-01 15:00:23", "2017-01-01 15:00:50", 5);
        Assert.assertEquals(blockedIpList.size(), blockedIpsResult.size());
    }

    public void initialRequestLogList() {
        RequestLog requestLog1 = new RequestLog();
        requestLog1.setSubmissionDate("2017-01-01 15:00:23.094");
        requestLog1.setSourceIp("192.168.228.5");
        requestLog1.setRequest("GET / HTTP/1.1");
        requestLog1.setStatus("200");
        requestLog1.setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36");

        RequestLog requestLog2 = new RequestLog();
        requestLog2.setSubmissionDate("2017-01-01 15:00:33.126");
        requestLog2.setSourceIp("192.168.106.181");
        requestLog2.setRequest("GET / HTTP/1.1");
        requestLog2.setStatus("200");
        requestLog2.setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36");

        RequestLog requestLog3 = new RequestLog();
        requestLog3.setSubmissionDate("2017-01-01 15:00:33.194");
        requestLog3.setSourceIp("192.168.226.6");
        requestLog3.setRequest("GET / HTTP/1.1");
        requestLog3.setStatus("200");
        requestLog3.setUserAgent("Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36");

        requestLogList.add(requestLog1);
        requestLogList.add(requestLog2);
        requestLogList.add(requestLog3);
    }

    public void initialBlockedIpList() {
        BlockedIp blockedIp1 = new BlockedIp();
        blockedIp1.setIp("192.168.203.111");
        blockedIp1.setReportStartDate("2017-01-01 15:00:00");
        blockedIp1.setReportEndDate("2017-01-01 16:00:00");
        blockedIp1.setHits(217l);
        blockedIp1.setBlockedDate("2017-11-12 18:55:16");
        blockedIp1.setBlockThreshold(200);

        BlockedIp blockedIp2 = new BlockedIp();
        blockedIp2.setIp("192.168.203.111");
        blockedIp2.setReportStartDate("2017-01-01 15:00:00");
        blockedIp2.setReportEndDate("2017-01-01 16:00:00");
        blockedIp2.setHits(217l);
        blockedIp2.setBlockedDate("2017-11-12 18:55:16");
        blockedIp2.setBlockThreshold(200);

        BlockedIp blockedIp3 = new BlockedIp();
        blockedIp3.setIp("192.168.203.111");
        blockedIp3.setReportStartDate("2017-01-01 15:00:00");
        blockedIp3.setReportEndDate("2017-01-01 16:00:00");
        blockedIp3.setHits(217l);
        blockedIp3.setBlockedDate("2017-11-12 18:55:16");
        blockedIp3.setBlockThreshold(200);

        blockedIpList.add(blockedIp1);
        blockedIpList.add(blockedIp2);
        blockedIpList.add(blockedIp3);
    }
}